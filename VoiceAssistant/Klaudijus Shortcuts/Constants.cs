using System;
using System.Collections.Generic;
using System.IO;

namespace ShortCuts
{
    static class Constants
    {
        public static readonly string ShortcutsFolder = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "Shortcuts");
        public static readonly string SystemFolder = Environment.SystemDirectory;
    }
}
